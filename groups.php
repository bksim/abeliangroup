<?
	//require_once("session.php");
	require_once("common.php");
	$user_id = $_SESSION["id"];
?>
<html>
	<head>
		<title>Groups</title>
		<link rel="stylesheet" href="http://twitter.github.com/bootstrap/1.4.0/bootstrap.min.css">	
		<link rel="stylesheet" href="events.css">
		
		<script type="text/javascript" src="/abeliangroup/res/jquery.js"></script>
		<script type="text/javascript" src="/abeliangroup/modal.js"></script>
		
		<script>
			$(document).ready(function()
			{
				$("#joinbutton").click(function(){
					location.href="join_group.php?groupid=" + $(this).attr("cid");
				});
				
				$("#grouplink_container_" + <?=$_GET["id"]?>).addClass("highlighted");
				
				$("option[value=<?=$_GET['id']?>]").attr("selected", "selected");
			});
		</script>
		
		<style type="text/css">
		body {
			padding-top: 60px;
		}
		</style>
	</head>

	<body>
		<!--top bar-->
		<?printBar();?>
		<?	$id = mysql_real_escape_string($_GET["id"]);
		$uid = $_SESSION["id"];
		?>
		<div class="container">
			<div class="row">
				<div class="span4 left">
		<?
		printGroups($uid);
		printGroupMembers($id, $uid);
		?>
			</div>
			<div class="span11 right">
		<?
		$query = "SELECT * FROM users INNER JOIN groupmemberships on groupmemberships.userid = users.id WHERE groupmemberships.groupid=$id AND users.id=$uid";
		$result = mysql_query($query);
		
		if (mysql_num_rows($result) == 0)
		{
		?>
			<div id="groupdiv_<?=$id?>" class="groupdiv">
		<?
			$query = "SELECT * FROM groups WHERE id = $id";
			$result = mysql_query($query);
			if (mysql_num_rows($result) == 0)
				echo "Group does not exist";
			else
			{
				$row = mysql_fetch_array($result);
				$name = $row["name"];
				$dsc = $row["dsc"];
		?>
				<div id="groupmaindiv_<?=$id?>" class="groupmaindiv">
					<div id="groupmaintop_<?=$id?>" class="groupmaintop">
						<div id="groupname_<?=$id?>" class="groupname"><?=$name?></div>
						<div id="groupdsc_<?=$id?>" class="groudsc"><?=$dsc?></div>
					</div>
				</div>
			<?
				echo "You are not a member of this group";
			?>
				<button class="btn" cid="<?=$id?>" id="joinbutton">Join Group</button>	
			<?
			}
			?>
			</div>
			<?
		}
		else
		{
			?>
			<a class="btn" data-controls-modal="event" data-backdrop="static">propose an event</a>
				<div id="event" class="modal hide fade" style="display: none; "> 
					<div class="modal-header">
						<a href="#" class="close">x</a> 
						<form name="input" action="create_event.php" method="post">
							<fieldset>
								<legend></br>propose an event</legend>
								
								<div class="clearfix">
									<label for="normalSelect">which group is this event for?</label>
									<div class="input">
										<select name="eventgroup" id="normalSelect"> <!--uses function in javascript at top of file-->
											<?getGroups($user_id);?>
										</select> 
									</div>
								</div>
								
								<div class="clearfix">
									<label for="xlInput">event name:</label>
									<div class="input">
										<input class="xlarge" id="xlInput" name="eventname" size="30" type="text">
									</div>
								</div>
								
								<div class="clearfix">
									<label for="textarea">event description: </label>
									<div class="input">
										<textarea class="xlarge" id="textarea2" name="eventdesc" rows="3"></textarea>
										<span class="help-block">
											what kind of event?
										</span>
									</div>
								</div>
								
								<div class="clearfix">
									<label for="normalSelect">"critical" number of likes:</label>
									<div class="input">
										<select name="crit_likes" id="normalSelect">
											<?printNumberDropDown();?>
										</select>
									</div>
								</div>
							</fieldset>
							
							<div class="modal-footer">
								<button class="btn primary" type="submit">Submit</button>
							</div>
						</form>	
					</div> 
				</div>
			<!--end of group modal-->
				
			<?	
			printGroupInfo($id, $uid); 
			?>
				<h2>Events</h2>
			<?
			printGroupEvents($id, $uid);
			?></div>
			</div>
			</div><?
		}
		?>
	</body>
	
</html>