<?
	require_once("common.php");
	$id = $_SESSION["id"];
?>
<html>
	<head>
		<title>Home Page</title>
		<link rel="stylesheet" href="http://twitter.github.com/bootstrap/1.4.0/bootstrap.min.css">
		<link rel="stylesheet" href="events.css">
	
		
		<script type="text/javascript" src="/abeliangroup/res/jquery.js"></script>
		<script type="text/javascript" src="/abeliangroup/modal.js"></script>
		
		<style type="text/css">
		body {
			padding-top: 60px;
		}
		</style>
	</head>
	
	<body>
	<!--to gray out the back-->
	<div id="mask"></div>
	
	<!--facebook javascript sdk code-->
	<div id="fb-root"></div>
	<script type="text/javascript" src="/abeliangroup/res/jquery.js"></script>
	<script>
		var userData;
		window.fbAsyncInit = function() {
			FB.init({
			  appId      : '359867544026606', // App ID
			  channelUrl : '//www.startpartying.net/abeliangroup/channel.php', // Channel File
			  status     : true, // check login status
			  cookie     : true, // enable cookies to allow the server to access the session
			  xfbml      : true,  // parse XFBML
			  oauth		 : true
			});
		
			FB.getLoginStatus(function(response) {
			  if (response.status === 'connected') {
				// the user is logged in and connected to your
				// app, and response.authResponse supplies
				// the user's ID, a valid access token, a signed
				// request, and the time the access token 
				// and signed request each expire
				var uid = response.authResponse.userID;
				var accessToken = response.authResponse.accessToken;
				
				// how to do without using global variable
				$.getJSON("https://graph.facebook.com/" + uid,
					function(data){
						userData = data;						
					});
				}
				
				else if (response.status === 'not_authorized') {
				// the user is logged in to Facebook, 
				//but not connected to the app
					location.href='index.html';
				} 
				else {
				// the user isn't even logged in to Facebook.
					location.href='index.html';
				}
			});
		};

		// Load the SDK Asynchronously
		(function(d){
			var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
			js = d.createElement('script'); js.id = id; js.async = true;
			js.src = "//connect.facebook.net/en_US/all.js";
			d.getElementsByTagName('head')[0].appendChild(js);
		}(document));						
	</script>
	<!--end facebook javascript sdk code-->	
	</body>
	
	
	<!--top bar-->
    <?printBar();?>
			
	<div class="container">
		<div class="row">
			<!--LEFTCOLUMN-->
			<div class="span4 left">
				<? printGroups($id);?>
				
				</br>
				<!--create a group modal-->
				<a class="btn" data-controls-modal="group" data-backdrop="static">create a group</a>
				<div id="group" class="modal hide fade" style="display: none; " > 
					<div class="modal-header">
						<a href="#" class="close">�</a> 
						<form name="input" action="register_group.php" method="post">
							<fieldset>
								<legend></br>create a group</legend>
								<div class="clearfix">
									<label for="xlInput">group name:</label>
									<div class="input">
										<input class="xlarge" id="xlInput" name="groupname" size="30" type="text">
									</div>
								</div>
								
								<div class="clearfix">
									<label for="textarea">group description: </label>
									<div class="input">
										<textarea class="xlarge" id="textarea2" name="groupdesc" rows="3"></textarea>
										<span class="help-block">
											describe your group. who's in it? what do you do?
										</span>
									</div>
								</div>
							</fieldset>
							
							<div class="modal-footer">
								<button class="btn primary" type="submit">Submit</button>
							</div>
						</form>	
					</div> 
				</div>
				<!--end of group modal-->
				

				</br>
				</br>
				<a href="suggestions.php" style="color:black;font-size:16">view suggested groups</a>
			</div>
				
				
			<!--RIGHTCOLUMN-->
			<div class="span11 right">				
				<!--create a group modal-->
				<a class="btn" data-controls-modal="event" data-backdrop="static">propose an event</a>
				<div id="event" class="modal hide fade" style="display: none; "> 
					<div class="modal-header">
						<a href="#" class="close">�</a> 
						<form name="input" action="create_event.php" method="post">
							<fieldset>
								<legend></br>propose an event</legend>
								
								<div class="clearfix">
									<label for="normalSelect">which group is this event for?</label>
									<div class="input">
										<select name="eventgroup" id="normalSelect"> <!--uses function in javascript at top of file-->
											<?getGroups($id);?>
										</select> 
									</div>
								</div>
								
								<div class="clearfix">
									<label for="xlInput">event name:</label>
									<div class="input">
										<input class="xlarge" id="xlInput" name="eventname" size="30" type="text">
									</div>
								</div>
								
								<div class="clearfix">
									<label for="textarea">event description: </label>
									<div class="input">
										<textarea class="xlarge" id="textarea2" name="eventdesc" rows="3"></textarea>
										<span class="help-block">
											what kind of event?
										</span>
									</div>
								</div>
								
								<div class="clearfix">
									<label for="normalSelect">"critical" number of likes:</label>
									<div class="input">
										<select name="crit_likes" id="normalSelect">
											<?printNumberDropDown();?>
										</select>
									</div>
								</div>
							</fieldset>
							
							<div class="modal-footer">
								<button class="btn primary" type="submit">Submit</button>
							</div>
						</form>	
					</div> 
				</div>
				<!--end of group modal-->
				
				<h4 id = "message2">or view existing events:</h4>
				<? printEvents($id); ?>
			</div>
		</div>
	</div>
</html>