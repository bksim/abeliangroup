<?
	require_once("session.php");
	require_once("common.php");
	
	$id = mysql_real_escape_string($_GET["id"]);
	$uid = $_SESSION["id"];
	
	$query = "select groupid from events inner join groupmemberships on events.groupmemid = groupmemberships.id WHERE events.id=$id";
	$result = mysql_query($query);
	$row = mysql_fetch_array($result);
	$groupid = $row["groupid"];
	//$eid = $_GET["id"];

?>

<html>
	<head>
		<title>Events</title>
		<link rel="stylesheet" href="http://twitter.github.com/bootstrap/1.4.0/bootstrap.min.css">	
		<link rel="stylesheet" href="events.css">
		
		<script>
			$(document).ready(function()
			{
				$("#grouplink_container_<?=$groupid?>").addClass("highlighted");		
			});
			</script>
		
		<script type="text/javascript" src="/abeliangroup/res/jquery.js"></script>
		<script type="text/javascript" src="/abeliangroup/modal.js"></script>
		
		<style type="text/css">
		body {
			padding-top: 60px;
		}
		</style>
	</head>
	
	<body>
		<!--top bar-->
		<?printBar();?>
		
		<div class="container">
			<div class="row">
				<div class="span4 left">
					<?	
					printGroups($uid);
					printEventLikers($id);
					?>
				</div>
				
				<div class="span11 right">
					<?
					$query = "SELECT * FROM events INNER JOIN groupmemberships AS ga ON ga.id = events.groupmemid INNER JOIN groupmemberships AS gb on ga.groupid = gb.groupid WHERE events.id = $id AND gb.userid = $uid";
					$result = mysql_query($query);
					
					if (mysql_num_rows($result) == 0)
					{
						echo "<div>You do not have the permission to see this event</div>";
						$query = "select groups.name AS gname, groupmemberships.groupid AS gmid, groupmemberships.id AS gid from events INNER JOIN groupmemberships on events.groupmemid = groupmemberships.id INNER JOIN groups ON groupmemberships.id = groups.id WHERE events.id = $id";
						$result = mysql_query($query);
						if (mysql_num_rows($result) == 0)
							echo "Group does not exist";
						else
						{
							$row = mysql_fetch_array($result);
							echo "Go to group: ";
							?>
								<div cid="<?=$row['gmid']?>" id="grouplink_<?=$row['gid']?>" class="grouplink"><?=$row["gname"]?></div>
							<?
						}
					}
					else
					{	
						printEvent($id, $uid, 1);
					}
					?>
				</div>
			</div>
		</div>
	</body>
</html>
